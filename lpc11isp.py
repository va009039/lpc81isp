# lpc11isp.py
# only LPC1114FN28 flash write program
import serial
import binascii
import time
import sys

RAM=0x10000400
CHUNK=256
SECTOR_SIZE=4096
CMD_SUCCESS="0"
LF='\n'
CRLF="\r\n"

class lpc11isp(object):
    def __init__(self, port, baudrate):
        self.ser = serial.Serial(port=port, baudrate=baudrate)

    def send(self, s):
        self.ser.write(s)

    def recv(self, n):
        return self.ser.read(n)

    def sendln(self, line):
        print "send:[%s]" % line
        self.send(line + CRLF)

    def waitln(self, s):
        line = ""
        timeout = time.time() + 3
        while time.time() < timeout:
            if self.ser.inWaiting() >= 1:
                c = self.ser.read(1)
                if c == LF:
                    print "recv:[%s]" % line
                    if self.ser.inWaiting() == 0 and line.endswith(s):
                        return
                    line = ""
                elif ord(c) >= 0x20:
                    line += c
        assert(0)

    def cmd(self, s):
        self.sendln(s)
        self.waitln(CMD_SUCCESS)

    def sync(self):
        self.send("?")
        self.waitln("ed") # Synchronized
        self.sendln("Synchronized")
        self.waitln("OK")
        self.sendln("12284")
        self.waitln("OK")
        self.cmd("A 0") # echo off

    def to_ram(self, data):
        self.cmd("W %d %d" % (RAM, CHUNK)) # write to ram
        cksum = 0
        line = 0
        for n in range(0, len(data), 45):
            d = data[n:n+45]
            s = binascii.b2a_uu(d).rstrip(LF) # uuencode
            self.sendln(s)
            for c in d:
                cksum += ord(c)
            line += 1
            if line >= 20 or n+len(d) >= CHUNK:
                self.sendln("%u" % cksum)
                self.waitln("OK")
                cksum = 0
                line = 0

    def flashWrite(self, data):
        self.sync()
        self.cmd("U 23130") # unlock
        for addr in range(0, len(data), CHUNK):
            d = (data[addr:addr+CHUNK]+'\xff'*CHUNK)[:CHUNK] # fill
            self.to_ram(d)
            if (addr % SECTOR_SIZE) == 0:
                sector = int(addr / SECTOR_SIZE)
                self.cmd("P %d %d" % (sector, sector))  # prepare sector
                self.cmd("E %d %d" % (sector, sector))  # erase sector
            self.cmd("P %d %d" % (sector, sector))
            self.cmd("C %d %d %d" % (addr, RAM, CHUNK)) # copy ram to flash

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--port', default='COM1')
    parser.add_argument('--baudrate', type=int, default=9600)
    parser.add_argument('binfile')
    args = parser.parse_args()
    filename = args.binfile
    with open(filename, "rb") as f:
        data = f.read()
    print("binfile: %s size: %d bytes" % (filename, len(data)))

    lpc1114 = lpc11isp(args.port, args.baudrate)
    lpc1114.flashWrite(data)
