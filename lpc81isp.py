# lpc81isp.py
# only LPC810 flash write program
import serial
import time
import sys

RAM=0x10000300
CHUNK=64
CMD_SUCCESS="0"

class lpc81isp(object):
    def __init__(self, port, baudrate):
        self.ser = serial.Serial(port=port, baudrate=baudrate)

    def send(self, s):
        self.ser.write(s)

    def recv(self, n):
        return self.ser.read(n)

    def sendln(self, line):
        print "send: %s" % line
        self.send(line + "\r\n")

    def waitln(self, s):
        for i in range(10):
            line = self.ser.readline().strip()
            print "recv: %s" % line
            if s == line:
                return
            time.sleep(0.2)
        sys.exit(1)

    def cmd(self, s):
        self.sendln(s)
        self.waitln(CMD_SUCCESS)

    def sync(self):
        self.send("?")
        self.waitln("Synchronized")
        self.sendln("Synchronized")
        self.waitln("OK")
        self.sendln("12284")
        self.waitln("OK")
        self.cmd("A 0") # echo off

    def to_ram(self, data):
        self.cmd("W %d %d" % (RAM, CHUNK)) # write to ram
        s = (data + '\xff'*CHUNK)[0:CHUNK] # fill chunk size
        self.send(s)

    def flashWrite(self, data):
        self.sync()
        self.cmd("U 23130") # unlock
        for addr in range(0, len(data), CHUNK):
            self.to_ram(data[addr:addr+CHUNK])
            if (addr % 1024) == 0:
                sector = int(addr / 1024)
                self.cmd("P %d %d" % (sector, sector))  # prepare sector
                self.cmd("E %d %d" % (sector, sector))  # erase sector
            self.cmd("P %d %d" % (sector, sector))
            self.cmd("C %d %d %d" % (addr, RAM, CHUNK)) # copy ram to flash

    def flashVerify(self, data):
        for addr in range(0, len(data), CHUNK):
            self.cmd("R %d %d" % (addr, CHUNK)) # read memory
            r = self.recv(CHUNK)
            assert(r.find(data[addr:addr+64]) == 0)

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--port', default='COM1')
    parser.add_argument('--baudrate', type=int, default=9600)
    parser.add_argument('binfile')
    args = parser.parse_args()
    filename = args.binfile
    with open(filename, "rb") as f:
        data = f.read()
    print("binfile: %s size: %d bytes" % (filename, len(data)))

    lpc810 = lpc81isp(args.port, args.baudrate)
    lpc810.flashWrite(data)
    lpc810.flashVerify(data)
